package encrypt

import (
	"bytes"
	"io"
	"testing"
)

func TestEncrypt(t *testing.T) {

	var (
		w      io.Writer
		n      [24]byte
		cipher []byte
	)

	passphrase := "PassPhrase-canopX#Encryption->TEST123"
	want := []byte("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")

	e := NewEncryptor(w, n, []byte(passphrase), 32)

	c := e.EncryptBlob(cipher, want)

	plainText, err := e.DecryptBlob(nil, c)
	if err != nil {
		t.Errorf("Decryption Failed: %s", err.Error())
	}

	if !bytes.Equal(plainText, want) {
		t.Errorf("GetServiceInfo() = %+v, want %+v", plainText, want)
	}

}
