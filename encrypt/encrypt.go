package encrypt

import (
	"crypto/rand"
	"errors"
	"io"

	"golang.org/x/crypto/nacl/secretbox"
	"golang.org/x/crypto/scrypt"
)

// Encryptor is an io.WriteCloser. Writes to an Encryptor are encrypted
// and written to w.
type Encryptor struct {
	writer io.Writer
	nonce  *[24]byte

	// Encryption key.
	key [32]byte

	chunkSize   int
	outputSlice []byte

	// Hooks for testing
	testRand func([]byte) (int, error)
}

// NewEncryptor creates an Encryptor with the default chunk size.
func NewEncryptor(writer io.Writer, nonce [24]byte, sharedKey []byte, chunkSize int) *Encryptor {
	//return &Encryptor{} //writer: writer, nonce: &nonce, key: sharedKey, chunkSize: DefaultChunkSize}
	e := &Encryptor{writer: writer, nonce: &nonce, chunkSize: chunkSize}
	e.setPassphrase(sharedKey)

	return e

}

// Format of encrypted blobs:
// versionByte (0x01) || 24 bytes nonce || secretbox(plaintext)
// The plaintext is long len(ciphertext) - 1 - 24 - secretbox.Overhead (16)

const version = 1

const overhead = 1 + 24 + secretbox.Overhead

// EncryptBlob encrypts plaintext and appends the result to ciphertext,
// which must not overlap plaintext.
func (e *Encryptor) EncryptBlob(ciphertext, plaintext []byte) []byte {
	if e.key == [32]byte{} {
		// Safety check, we really don't want this to happen.
		panic("no passphrase set")
	}
	var nonce [24]byte
	e.randNonce(&nonce)
	ciphertext = append(ciphertext, version)
	ciphertext = append(ciphertext, nonce[:]...)
	return secretbox.Seal(ciphertext, plaintext, &nonce, &e.key)
}

// DecryptBlob decrypts ciphertext and appends the result to plaintext,
// which must not overlap ciphertext.
func (e *Encryptor) DecryptBlob(plaintext, ciphertext []byte) ([]byte, error) {
	if len(ciphertext) < overhead {
		return nil, errors.New("blob too short to be encrypted")
	}
	if ciphertext[0] != version {
		return nil, errors.New("unknown encrypted blob version")
	}
	var nonce [24]byte
	copy(nonce[:], ciphertext[1:])
	plaintext, success := secretbox.Open(plaintext, ciphertext[25:], &nonce, &e.key)
	if !success {
		return nil, errors.New("encrypted blob failed authentication")
	}
	return plaintext, nil
}

var scryptN = 1 << 20 // DO NOT change, except in tests

func (e *Encryptor) setPassphrase(passphrase []byte) {
	if len(passphrase) == 0 {
		panic("tried to set empty passphrase")
	}

	// We can't use a random salt as the passphrase wouldn't be enough to recover the
	// data anymore, but we use a custom one so that generic tables are useless.
	salt := []byte("canopX")

	// "Sensitive storage" reccomended parameters. 5s in 2009, probably less now.
	// https://www.tarsnap.com/scrypt/scrypt-slides.pdf
	key, err := scrypt.Key(passphrase, salt, scryptN, 8, 1, 32)
	if err != nil {
		// This can't happen with good parameters, which are fixed.
		panic("scrypt key derivation failed: " + err.Error())
	}

	if copy(e.key[:], key) != 32 {
		panic("copied wrong key length")
	}
}

func (e *Encryptor) randNonce(nonce *[24]byte) {
	rand := rand.Read
	if e.testRand != nil {
		rand = e.testRand
	}
	_, err := rand(nonce[:])
	if err != nil {
		panic(err)
	}
}
