// Code generated by protoc-gen-go. DO NOT EDIT.
// source: datastore.proto

package canopx_datastore_v1

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CreateBlobResponse_UploadStatus int32

const (
	CreateBlobResponse_UPLOAD_STATUS_UNSPECIFIED CreateBlobResponse_UploadStatus = 0
	CreateBlobResponse_SUCCESS                   CreateBlobResponse_UploadStatus = 1
	CreateBlobResponse_FAILED                    CreateBlobResponse_UploadStatus = 2
)

var CreateBlobResponse_UploadStatus_name = map[int32]string{
	0: "UPLOAD_STATUS_UNSPECIFIED",
	1: "SUCCESS",
	2: "FAILED",
}

var CreateBlobResponse_UploadStatus_value = map[string]int32{
	"UPLOAD_STATUS_UNSPECIFIED": 0,
	"SUCCESS":                   1,
	"FAILED":                    2,
}

func (x CreateBlobResponse_UploadStatus) String() string {
	return proto.EnumName(CreateBlobResponse_UploadStatus_name, int32(x))
}

func (CreateBlobResponse_UploadStatus) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{1, 0}
}

type Blob_Type int32

const (
	Blob_TYPE_UNSPECIFIED Blob_Type = 0
	Blob_META             Blob_Type = 1
	Blob_BYTES            Blob_Type = 2
)

var Blob_Type_name = map[int32]string{
	0: "TYPE_UNSPECIFIED",
	1: "META",
	2: "BYTES",
}

var Blob_Type_value = map[string]int32{
	"TYPE_UNSPECIFIED": 0,
	"META":             1,
	"BYTES":            2,
}

func (x Blob_Type) String() string {
	return proto.EnumName(Blob_Type_name, int32(x))
}

func (Blob_Type) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{3, 0}
}

type CreateBlobRequest struct {
	Blob                 *Blob    `protobuf:"bytes,1,opt,name=blob,proto3" json:"blob,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateBlobRequest) Reset()         { *m = CreateBlobRequest{} }
func (m *CreateBlobRequest) String() string { return proto.CompactTextString(m) }
func (*CreateBlobRequest) ProtoMessage()    {}
func (*CreateBlobRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{0}
}

func (m *CreateBlobRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateBlobRequest.Unmarshal(m, b)
}
func (m *CreateBlobRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateBlobRequest.Marshal(b, m, deterministic)
}
func (m *CreateBlobRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateBlobRequest.Merge(m, src)
}
func (m *CreateBlobRequest) XXX_Size() int {
	return xxx_messageInfo_CreateBlobRequest.Size(m)
}
func (m *CreateBlobRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateBlobRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateBlobRequest proto.InternalMessageInfo

func (m *CreateBlobRequest) GetBlob() *Blob {
	if m != nil {
		return m.Blob
	}
	return nil
}

type CreateBlobResponse struct {
	UploadStatus         CreateBlobResponse_UploadStatus `protobuf:"varint,1,opt,name=upload_status,json=uploadStatus,proto3,enum=canopx.datastore.v1.CreateBlobResponse_UploadStatus" json:"upload_status,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                        `json:"-"`
	XXX_unrecognized     []byte                          `json:"-"`
	XXX_sizecache        int32                           `json:"-"`
}

func (m *CreateBlobResponse) Reset()         { *m = CreateBlobResponse{} }
func (m *CreateBlobResponse) String() string { return proto.CompactTextString(m) }
func (*CreateBlobResponse) ProtoMessage()    {}
func (*CreateBlobResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{1}
}

func (m *CreateBlobResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateBlobResponse.Unmarshal(m, b)
}
func (m *CreateBlobResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateBlobResponse.Marshal(b, m, deterministic)
}
func (m *CreateBlobResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateBlobResponse.Merge(m, src)
}
func (m *CreateBlobResponse) XXX_Size() int {
	return xxx_messageInfo_CreateBlobResponse.Size(m)
}
func (m *CreateBlobResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateBlobResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateBlobResponse proto.InternalMessageInfo

func (m *CreateBlobResponse) GetUploadStatus() CreateBlobResponse_UploadStatus {
	if m != nil {
		return m.UploadStatus
	}
	return CreateBlobResponse_UPLOAD_STATUS_UNSPECIFIED
}

type GetBlobRequest struct {
	Ref                  string   `protobuf:"bytes,1,opt,name=ref,proto3" json:"ref,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetBlobRequest) Reset()         { *m = GetBlobRequest{} }
func (m *GetBlobRequest) String() string { return proto.CompactTextString(m) }
func (*GetBlobRequest) ProtoMessage()    {}
func (*GetBlobRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{2}
}

func (m *GetBlobRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetBlobRequest.Unmarshal(m, b)
}
func (m *GetBlobRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetBlobRequest.Marshal(b, m, deterministic)
}
func (m *GetBlobRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetBlobRequest.Merge(m, src)
}
func (m *GetBlobRequest) XXX_Size() int {
	return xxx_messageInfo_GetBlobRequest.Size(m)
}
func (m *GetBlobRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetBlobRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetBlobRequest proto.InternalMessageInfo

func (m *GetBlobRequest) GetRef() string {
	if m != nil {
		return m.Ref
	}
	return ""
}

type Blob struct {
	// The sha224 hash of the blob
	Ref                  string    `protobuf:"bytes,1,opt,name=ref,proto3" json:"ref,omitempty"`
	Data                 []byte    `protobuf:"bytes,2,opt,name=data,proto3" json:"data,omitempty"`
	Number               int64     `protobuf:"varint,3,opt,name=number,proto3" json:"number,omitempty"`
	Size                 int64     `protobuf:"varint,4,opt,name=size,proto3" json:"size,omitempty"`
	Type                 Blob_Type `protobuf:"varint,5,opt,name=type,proto3,enum=canopx.datastore.v1.Blob_Type" json:"type,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *Blob) Reset()         { *m = Blob{} }
func (m *Blob) String() string { return proto.CompactTextString(m) }
func (*Blob) ProtoMessage()    {}
func (*Blob) Descriptor() ([]byte, []int) {
	return fileDescriptor_d08157cfd31fc929, []int{3}
}

func (m *Blob) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Blob.Unmarshal(m, b)
}
func (m *Blob) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Blob.Marshal(b, m, deterministic)
}
func (m *Blob) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Blob.Merge(m, src)
}
func (m *Blob) XXX_Size() int {
	return xxx_messageInfo_Blob.Size(m)
}
func (m *Blob) XXX_DiscardUnknown() {
	xxx_messageInfo_Blob.DiscardUnknown(m)
}

var xxx_messageInfo_Blob proto.InternalMessageInfo

func (m *Blob) GetRef() string {
	if m != nil {
		return m.Ref
	}
	return ""
}

func (m *Blob) GetData() []byte {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *Blob) GetNumber() int64 {
	if m != nil {
		return m.Number
	}
	return 0
}

func (m *Blob) GetSize() int64 {
	if m != nil {
		return m.Size
	}
	return 0
}

func (m *Blob) GetType() Blob_Type {
	if m != nil {
		return m.Type
	}
	return Blob_TYPE_UNSPECIFIED
}

func init() {
	proto.RegisterEnum("canopx.datastore.v1.CreateBlobResponse_UploadStatus", CreateBlobResponse_UploadStatus_name, CreateBlobResponse_UploadStatus_value)
	proto.RegisterEnum("canopx.datastore.v1.Blob_Type", Blob_Type_name, Blob_Type_value)
	proto.RegisterType((*CreateBlobRequest)(nil), "canopx.datastore.v1.CreateBlobRequest")
	proto.RegisterType((*CreateBlobResponse)(nil), "canopx.datastore.v1.CreateBlobResponse")
	proto.RegisterType((*GetBlobRequest)(nil), "canopx.datastore.v1.GetBlobRequest")
	proto.RegisterType((*Blob)(nil), "canopx.datastore.v1.Blob")
}

func init() { proto.RegisterFile("datastore.proto", fileDescriptor_d08157cfd31fc929) }

var fileDescriptor_d08157cfd31fc929 = []byte{
	// 395 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x52, 0xd1, 0x8e, 0xd2, 0x40,
	0x14, 0x65, 0xa0, 0xcb, 0xba, 0x77, 0x71, 0x1d, 0xaf, 0xc6, 0x74, 0x37, 0xd1, 0x90, 0x31, 0xd1,
	0xbe, 0xd8, 0xb8, 0xd5, 0x1f, 0x28, 0x6d, 0x31, 0x24, 0xa0, 0xa4, 0xd3, 0x3e, 0xf0, 0x44, 0xa6,
	0x30, 0x26, 0x24, 0xc8, 0xd4, 0x76, 0x4a, 0xc4, 0x1f, 0xf3, 0xd1, 0x17, 0x3f, 0xcc, 0x74, 0x42,
	0x42, 0x15, 0x88, 0xbe, 0xdd, 0x3b, 0xf7, 0x9c, 0x93, 0x39, 0xe7, 0x5e, 0x78, 0xb4, 0x14, 0x5a,
	0x94, 0x5a, 0x15, 0xd2, 0xcd, 0x0b, 0xa5, 0x15, 0x3e, 0x59, 0x88, 0x8d, 0xca, 0xbf, 0xb9, 0x87,
	0xf7, 0xed, 0x3d, 0x1b, 0xc0, 0xe3, 0xa0, 0x90, 0x42, 0xcb, 0xc1, 0x5a, 0x65, 0xb1, 0xfc, 0x5a,
	0xc9, 0x52, 0xe3, 0x1b, 0xb0, 0xb2, 0xb5, 0xca, 0x6c, 0xd2, 0x27, 0xce, 0xb5, 0x77, 0xeb, 0x9e,
	0x20, 0xba, 0x06, 0x6f, 0x60, 0xec, 0x07, 0x01, 0x6c, 0x8a, 0x94, 0xb9, 0xda, 0x94, 0x12, 0x67,
	0xf0, 0xb0, 0xca, 0xd7, 0x4a, 0x2c, 0xe7, 0xa5, 0x16, 0xba, 0x2a, 0x8d, 0xdc, 0x8d, 0xf7, 0xfe,
	0xa4, 0xdc, 0x31, 0xdf, 0x4d, 0x0d, 0x99, 0x1b, 0x6e, 0xdc, 0xab, 0x1a, 0x1d, 0x1b, 0x42, 0xaf,
	0x39, 0xc5, 0xe7, 0x70, 0x9b, 0x4e, 0xc7, 0x9f, 0xfc, 0x70, 0xce, 0x13, 0x3f, 0x49, 0xf9, 0x3c,
	0xfd, 0xc8, 0xa7, 0x51, 0x30, 0x1a, 0x8e, 0xa2, 0x90, 0xb6, 0xf0, 0x1a, 0x2e, 0x79, 0x1a, 0x04,
	0x11, 0xe7, 0x94, 0x20, 0x40, 0x77, 0xe8, 0x8f, 0xc6, 0x51, 0x48, 0xdb, 0x8c, 0xc1, 0xcd, 0x07,
	0xa9, 0x9b, 0xd6, 0x29, 0x74, 0x0a, 0xf9, 0xd9, 0x7c, 0xf5, 0x2a, 0xae, 0x4b, 0xf6, 0x93, 0x80,
	0x55, 0x23, 0x8e, 0x47, 0x88, 0x60, 0xd5, 0x26, 0xec, 0x76, 0x9f, 0x38, 0xbd, 0xd8, 0xd4, 0xf8,
	0x0c, 0xba, 0x9b, 0xea, 0x4b, 0x26, 0x0b, 0xbb, 0xd3, 0x27, 0x4e, 0x27, 0xde, 0x77, 0x35, 0xb6,
	0x5c, 0x7d, 0x97, 0xb6, 0x65, 0x5e, 0x4d, 0x8d, 0x1e, 0x58, 0x7a, 0x97, 0x4b, 0xfb, 0xc2, 0x04,
	0xf3, 0xe2, 0x6c, 0xce, 0x6e, 0xb2, 0xcb, 0x65, 0x6c, 0xb0, 0xec, 0x1e, 0xac, 0xba, 0xc3, 0xa7,
	0x40, 0x93, 0xd9, 0x34, 0xfa, 0xcb, 0xe9, 0x03, 0xb0, 0x26, 0x51, 0xe2, 0x53, 0x82, 0x57, 0x70,
	0x31, 0x98, 0x25, 0x11, 0xa7, 0x6d, 0xef, 0x17, 0x01, 0x1a, 0x0a, 0x2d, 0x78, 0xad, 0xc9, 0x65,
	0xb1, 0x5d, 0x2d, 0x24, 0x0a, 0x80, 0x43, 0xe6, 0xf8, 0xea, 0x9f, 0x4b, 0x31, 0xf1, 0xdc, 0xbd,
	0xfe, 0xcf, 0xe5, 0xb1, 0x96, 0x43, 0x70, 0x02, 0x97, 0xfb, 0x74, 0xf1, 0xe5, 0x49, 0xde, 0x9f,
	0xd9, 0xdf, 0x9d, 0x3f, 0x34, 0xd6, 0x7a, 0x4b, 0xb2, 0xae, 0x39, 0xe3, 0x77, 0xbf, 0x03, 0x00,
	0x00, 0xff, 0xff, 0x38, 0xe3, 0xb4, 0x5a, 0xd9, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// DataStoreServiceClient is the client API for DataStoreService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DataStoreServiceClient interface {
	CreateBlob(ctx context.Context, opts ...grpc.CallOption) (DataStoreService_CreateBlobClient, error)
	GetBlob(ctx context.Context, in *GetBlobRequest, opts ...grpc.CallOption) (DataStoreService_GetBlobClient, error)
}

type dataStoreServiceClient struct {
	cc *grpc.ClientConn
}

func NewDataStoreServiceClient(cc *grpc.ClientConn) DataStoreServiceClient {
	return &dataStoreServiceClient{cc}
}

func (c *dataStoreServiceClient) CreateBlob(ctx context.Context, opts ...grpc.CallOption) (DataStoreService_CreateBlobClient, error) {
	stream, err := c.cc.NewStream(ctx, &_DataStoreService_serviceDesc.Streams[0], "/canopx.datastore.v1.DataStoreService/CreateBlob", opts...)
	if err != nil {
		return nil, err
	}
	x := &dataStoreServiceCreateBlobClient{stream}
	return x, nil
}

type DataStoreService_CreateBlobClient interface {
	Send(*CreateBlobRequest) error
	CloseAndRecv() (*CreateBlobResponse, error)
	grpc.ClientStream
}

type dataStoreServiceCreateBlobClient struct {
	grpc.ClientStream
}

func (x *dataStoreServiceCreateBlobClient) Send(m *CreateBlobRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *dataStoreServiceCreateBlobClient) CloseAndRecv() (*CreateBlobResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(CreateBlobResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *dataStoreServiceClient) GetBlob(ctx context.Context, in *GetBlobRequest, opts ...grpc.CallOption) (DataStoreService_GetBlobClient, error) {
	stream, err := c.cc.NewStream(ctx, &_DataStoreService_serviceDesc.Streams[1], "/canopx.datastore.v1.DataStoreService/GetBlob", opts...)
	if err != nil {
		return nil, err
	}
	x := &dataStoreServiceGetBlobClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type DataStoreService_GetBlobClient interface {
	Recv() (*Blob, error)
	grpc.ClientStream
}

type dataStoreServiceGetBlobClient struct {
	grpc.ClientStream
}

func (x *dataStoreServiceGetBlobClient) Recv() (*Blob, error) {
	m := new(Blob)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// DataStoreServiceServer is the server API for DataStoreService service.
type DataStoreServiceServer interface {
	CreateBlob(DataStoreService_CreateBlobServer) error
	GetBlob(*GetBlobRequest, DataStoreService_GetBlobServer) error
}

// UnimplementedDataStoreServiceServer can be embedded to have forward compatible implementations.
type UnimplementedDataStoreServiceServer struct {
}

func (*UnimplementedDataStoreServiceServer) CreateBlob(srv DataStoreService_CreateBlobServer) error {
	return status.Errorf(codes.Unimplemented, "method CreateBlob not implemented")
}
func (*UnimplementedDataStoreServiceServer) GetBlob(req *GetBlobRequest, srv DataStoreService_GetBlobServer) error {
	return status.Errorf(codes.Unimplemented, "method GetBlob not implemented")
}

func RegisterDataStoreServiceServer(s *grpc.Server, srv DataStoreServiceServer) {
	s.RegisterService(&_DataStoreService_serviceDesc, srv)
}

func _DataStoreService_CreateBlob_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(DataStoreServiceServer).CreateBlob(&dataStoreServiceCreateBlobServer{stream})
}

type DataStoreService_CreateBlobServer interface {
	SendAndClose(*CreateBlobResponse) error
	Recv() (*CreateBlobRequest, error)
	grpc.ServerStream
}

type dataStoreServiceCreateBlobServer struct {
	grpc.ServerStream
}

func (x *dataStoreServiceCreateBlobServer) SendAndClose(m *CreateBlobResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *dataStoreServiceCreateBlobServer) Recv() (*CreateBlobRequest, error) {
	m := new(CreateBlobRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _DataStoreService_GetBlob_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(GetBlobRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(DataStoreServiceServer).GetBlob(m, &dataStoreServiceGetBlobServer{stream})
}

type DataStoreService_GetBlobServer interface {
	Send(*Blob) error
	grpc.ServerStream
}

type dataStoreServiceGetBlobServer struct {
	grpc.ServerStream
}

func (x *dataStoreServiceGetBlobServer) Send(m *Blob) error {
	return x.ServerStream.SendMsg(m)
}

var _DataStoreService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "canopx.datastore.v1.DataStoreService",
	HandlerType: (*DataStoreServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "CreateBlob",
			Handler:       _DataStoreService_CreateBlob_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "GetBlob",
			Handler:       _DataStoreService_GetBlob_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "datastore.proto",
}
