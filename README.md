# DataStore

Indexes, encrypts & stores content

## Dependencies

- MongoDB: used for Indexing metadata

- MinIO: used to store files

## Build from source code

```shell
cd ./api/v1
protoc -I. --gopherjs_out=plugins=grpc,,Mgoogle/protobuf/timestamp.proto=github.com/johanbrandhorst/protobuf/vendor/google.golang.org/grpc/metadata:client --go_out=plugins=grpc:server ds.proto
```

```shell
go install
```

## Init

```bash
datastore init
```

This will create the file `config.yml` in the folder `~/.config/canopx/datastore`

## Configuration

## Encryption

canopX uses the secretbox package of golang: https://godoc.org/golang.org/x/crypto/nacl/secretbox

Secretbox uses XSalsa20 and Poly1305 to encrypt and authenticate messages with secret-key cryptography. The length of messages is not hidden.

A strong password is asked during the init phase but it can be modified in the configuration file:

```yaml
storage:
  passphrase: ''
```

## JWT key for authentication

```yaml
auth:
  jwtpublickeypem: ''
```

## TLS config

Complete this part of the configuration file:

```yaml
grpc:
  addr: 'localhost:50051'
  tlscert: ''
  tlskey: ''
```



## Public APIs

- CreateBlob

- ListBlobs

- GetBlobDetails
  
  Only to retrieve the metadata (references, names, tags, datetime, and sizes)

- GetBlobChunks


